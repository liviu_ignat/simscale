## Universal simscale ReactJS sample project

**Public url: http://simscale.herokuapp.com/**

#### Features
* Uses SimScale API similar to current production usage

* Search projects

* lazy-loading by pressing "Load More" button on the bottom

* Responsive - responsive list, header search + button, footer, header menu (drawer menu for mobile view) 

* Compatible with Chrome, Firefox, Safari, IE10+

#### Technical keywords:

* universal javascript - read bellow

* ReactJS, redux, ES 6/7 - technologies of today and the future

* webpack - live dev reloading with webpack, sass, JSON, image modules and a lot of development boosts

* sass modules - awesome new way to organise component drive CSS/SASS

* flex-box - native CSS grid system, support for IE10+ and all modern browsers


#### Why universal rendering:
* Universal javascript is Javascript that is capable to run both on the server and the client. The biggest dependency is not to have DOM dependent code like jQuery.

* Faster loading speed, especially mobile

* SEO for free - click view source and you will see all results there

* more security, eg: forbidden routes are redirected from server, from server-side React Router

* complete freedom to decide what to render on server and what to render on client only


#### Sass Modules 
* the ability to require sass files and (most important) have isolated scope for each component file

####  Short project structure description:

 * `./src/reducer.js` - main reducer

 * `./src/routes.js` - main app routes

 * `./src/express-app.js` - express app middlware that handles server-side rendering

 * `./src/client.js` - client app bootstrapper

 * `./src/theme` - main folder with fontawesome config and SASS variables / mixins

 * `./src/helpers` - some helper utilities

 * `./src/main` - ReactJS common and domain components, split in ReactJS pages - redux top level smart components, and normal components

 * `./src/main/product/__tests__/` - some tests

#### Development prerequisites
**NodeJS 5.x** or higher installed (hopefully you have `nvm` installed)

#### Useful development commands
```
npm install       // install app
npm run dev       // run in dev
npm run test      // run all tests
npm run build     // build for production
npm run start     // start in production
```

#### Start using the project in development
* navigate to http://localhost:3000

#### Current known issues
* on IE9 flex-box is buggy

* on IE10 a javascript error is thrown on load from a third party library (`redux-async-connect`) - still looking for alternatives

* on Safari and mobile Safari the bottom links are not responsive - still have to investigate