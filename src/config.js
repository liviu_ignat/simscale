require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  port: process.env.PORT || 3000,
  app: {
    baseApiUrl: 'https://www.simscale.com',
    productPageSize: 40,
    title: 'SimScale Platform - Simulation in your Browser',
    meta: [
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {name: 'description', content: 'SimScale Platform - Simulation in your Browser'},
      {name: 'robots', content: 'index,follow'},
      {name: 'Content-Type', content: 'text/html; charset=utf-8'},
      {name: 'Copyright', content: 'liviu@ignat.email'},
      {name: 'audience', content: 'all'},
      {charset: 'utf-8'},
      {property: 'keywords', content: 'simulation, cloud computing, cloud services, cloud based services'},
      {property: 'og:site_name', content: 'SimScale'},
      {property: 'og:locale', content: 'en_US'},
      {property: 'og:title', content: 'Marketplace, Billing and Distribution, Reseller, and Management Services - AppDirect'},
      {property: 'og:description', content: 'SimScale Platform - Simulation in your Browser'},
      {property: 'og:image', content: 'https://clqtg10snjb14i85u49wifbv-wpengine.netdna-ssl.com/wp-content/uploads/2015/07/SimScale-platform.png'},
      {property: 'og:image:width', content: '646'},
      {property: 'og:image:height', content: '220'},
    ]
  }
}, environment);
