import React, {Component, PropTypes} from 'react';
import {Divider, IconButton, IconMenu, MenuItem, SvgIcon} from 'main/common/components';

export default class AppHeader extends Component {
  static propTypes = {
    toggleNavigationDrawer: PropTypes.func.isRequired,
  };

  openNavigationDrawer() {
    this.props.toggleNavigationDrawer(true);
  }

  render() {
    const css = require('./AppHeader.scss');
    const userLabel = (<span>
      <span className={css.ProfileNavigationItem_username}>lignat</span>
      <span className="fa fa-chevron-down"></span>
    </span>);

    return (
      <header className={css.AppHeader}>
        <div className={css.DrawerIcon}>
          <IconButton onClick={::this.openNavigationDrawer}>
            <SvgIcon color="black">
              <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
            </SvgIcon>
          </IconButton>
        </div>

        <a className={css.AppLogoLink} src="http://www.simscale.com">
          <img src="https://www.simscale.com/community/assets/img/logo.png" alt=""/>
        </a>

        <ul className={css.AppNavigation}>
          <li className={css.LinkNavigationItem}>
            <a href="https://www.simscale.com/dashboard/">Dashboard</a>
          </li>
          <li className={css.LinkNavigationItem_selected}>
            <a href="/">Public Projects</a>
          </li>
          <li className={css.LinkNavigationItem}>
            <a href="https://www.simscale.com/forum/">Forum</a>
          </li>
          <li className={css.ProfileNavigationItem}>
            <IconMenu
              className={css.GiftRow_menu}
              iconButtonElement={userLabel}
              anchorOrigin={{horizontal: 'right', vertical: 'top'}}
              targetOrigin={{horizontal: 'right', vertical: 'top'}}>
              <MenuItem primaryText={<a className={css.HeaderNavigation_link} href="https://www.simscale.com/users/lignat/">View profile</a>} />
              <MenuItem primaryText={<a className={css.HeaderNavigation_link} href="https://www.simscale.com/users/lignat/">Manage account</a>} />
              <Divider />
              <MenuItem primaryText="Log out" />
            </IconMenu>
          </li>
        </ul>
      </header>
    );
  }
}
