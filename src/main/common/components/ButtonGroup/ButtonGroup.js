import React, {Component, PropTypes} from 'react';

export default class ButtonGroup extends Component {
  static propTypes = {
    children: PropTypes.any.isRequired
  };

  render() {
    const css = require('./ButtonGroup.scss');
    const {children} = this.props;

    return (
      <div {...this.props} className={css.ButtonGroup}>
        {children}
      </div>
    );
  }
}
