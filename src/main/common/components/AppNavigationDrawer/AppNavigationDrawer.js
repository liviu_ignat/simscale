import React, {Component, PropTypes} from 'react';
import {LeftNav} from 'material-ui';
import {Divider, MenuItem, Card, CardMedia, CardTitle} from 'main/common/components';

export default class AppNavigationDrawer extends Component {
  static propTypes = {
    isNavDrawerOpened: PropTypes.bool.isRequired,
    pushState: PropTypes.func.isRequired,
    toggleNavigationDrawer: PropTypes.func.isRequired
  };

  close() {
    const {toggleNavigationDrawer} = this.props;
    toggleNavigationDrawer(false);
  }

  render() {
    const css = require('./AppNavigationDrawer.scss');
    const {isNavDrawerOpened} = this.props;

    return (
      <LeftNav
        className={css.AppNavigationDrawer}
        width={220}
        docked={false}
        open={isNavDrawerOpened}
        onRequestChange={open => this.props.toggleNavigationDrawer(open)}>
        <div style={{cursor: 'pointer'}}>
          <Card>
            <CardMedia
              overlay={<CardTitle subtitle="Liviu Ignat"/>}>
              <img src="https://www.gravatar.com/avatar/ea2654ee1c81f26f378ab7d62ebcb898?s=200" />
            </CardMedia>
          </Card>
        </div>

        <Divider />

        <MenuItem>
          <a className={css.AppNavigationDrawer_link} href="https://www.simscale.com/users/lignat/">
            View profile
          </a>
        </MenuItem>

        <MenuItem>
          <a className={css.AppNavigationDrawer_link} href="https://www.simscale.com/users/lignat/profile/edit/">
            View profile
          </a>
        </MenuItem>

        <Divider />

        <MenuItem>
          <span className={css.AppNavigationDrawer_link}>
            Logout
          </span>
        </MenuItem>
      </LeftNav>
    );
  }
}
