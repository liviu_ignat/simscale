import React, {Component} from 'react';
import {TextFieldGroup} from 'main/common/components';

export default class AppFooter extends Component {

  renderLinks(data = []) {
    return (
      <ul>
        {data.map((item, index) => <li key={index}><a href="href: 'https://www.simscale.com">{item.text}</a></li>)}
      </ul>
    );
  }

  render() {
    const css = require('./AppFooter.scss');

    return (
      <footer className={css.AppFooter}>
        <div className={css.AppFooterContainer}>
          <div className={css.LinksContainer}>
            <div className={css.LinkRow}>
              <h4>Product</h4>
              {this.renderLinks([
                {text: 'Product Tour'},
                {text: 'Simulation Features'},
                {text: 'Benefits'},
                {text: 'Plans & Pricing'}
              ])}
            </div>

            <div className={css.LinkRow}>
              <h4>Services</h4>
              {this.renderLinks([
                {text: 'Support'},
                {text: 'Blog'},
                {text: 'Forum'}
              ])}
            </div>

            <div className={css.LinkRow}>
              <h4>Resources</h4>
              {this.renderLinks([
                {text: 'Public Projects'},
                {text: 'Documentation'},
                {text: 'Workshops'}
              ])}
            </div>

            <div className={css.LinkRow}>
              <h4>Company</h4>
              {this.renderLinks([
                {text: 'Careers'},
                {text: 'Press'},
                {text: 'Contact Us'},
                {text: 'Terms & Conditions'},
                {text: 'Data Privacy'},
                {text: 'Imprint'}
              ])}
            </div>
          </div>

          <div className={css.InfoContainer}>
            <h4 className={css.NewsletterRow}>Stay informed about SimScale products and sign up for our newsletter.</h4>

            <div className={css.NewsletterRow}>
              <TextFieldGroup
                inputPlaceholder="Your email address"
                buttonLabel={<span>SIGN UP</span>} />
            </div>

            <h4 className={css.NewsletterRow}>Get social with SimScale.</h4>

            <div className={css.NewsletterRow}>
              <a className={css.SocialLink}
                href="https://www.xing.com/companies/simscalegmbh" target="_blank" rel="nofollow">
                <span><i className={css.IconXing} /></span>
              </a>
              <a className={css.SocialLink}
                href="https://www.linkedin.com/company/2864479" target="_blank" rel="nofollow">
                <span><i className={css.IconLinkedIn} /></span>
              </a>
              <a className={css.SocialLink}
                href="https://twitter.com/simscale" target="_blank" rel="nofollow">
                <span><i className={css.IconTwitter} /></span>
              </a>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
