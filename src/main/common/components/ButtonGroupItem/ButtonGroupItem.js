import React, {Component, PropTypes} from 'react';

export default class ButtonGroupItem extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };

  render() {
    const css = require('./ButtonGroupItem.scss');
    const {children} = this.props;

    return (
      <button {...this.props} className={css.ButtonGroupItem}>
        {children}
      </button>
    );
  }
}
