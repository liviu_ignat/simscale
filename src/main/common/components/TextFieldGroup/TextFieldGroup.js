import React, {Component, PropTypes} from 'react';

export default class TextFieldGroup extends Component {
  static propTypes = {
    value: PropTypes.string,
    inputPlaceholder: PropTypes.string,
    buttonLabel: PropTypes.any,
    color: PropTypes.string,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func
  };

  static defaultProps = {
    value: '',
    color: '',
    inputPlaceholder: '',
    buttonLabel: 'Button'
  }

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.state = {value};
  }

  onChange(evt) {
    const {target: {value}} = evt;

    this.setState({value});

    if (this.props.onChange) {
      this.props.onChange(evt);
    }
  }

  onKeyPress(evt) {
    if (evt.key === 'Enter' && this.props.onSubmit) {
      this.props.onSubmit(this.state.value);
    }
  }

  onSubmit() {
    if (this.props.onSubmit) {
      this.props.onSubmit(this.state.value);
    }
  }

  render() {
    const css = require('./TextFieldGroup.scss');
    const {inputPlaceholder, buttonLabel} = this.props;
    const {value} = this.state;

    return (
      <div className={css.TextFieldGroup}>
        <input className={css.InputField}
          value={value}
          placeholder={inputPlaceholder}
          onKeyPress={::this.onKeyPress}
          onChange={::this.onChange}/>
        <button className={css.Button}
          style={this.style}
          onClick={::this.onSubmit}>
          {buttonLabel}
        </button>
      </div>
    );
  }
}
