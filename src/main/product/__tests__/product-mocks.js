export const PRODUCT_LIST_MOCK = [
  {
    projectName: "Bearing block analysis",
    publicProjectName: "bearingblock-analysis",
    creationDate: "Jul 9, 2013 5:03:42 PM",
    lastModificationDate: "Jan 28, 2016 2:06:57 PM",
    ownerName: "simscale",
    measurementSystem: "SI",
    requesterPermission: {
      readPreview: false,
      read: false,
      write: false,
      billableAction: false,
      getCopy: false
    },
    publicPermission: {
      readPreview: true,
      read: true,
      write: false,
      billableAction: false,
      getCopy: true
    },
    description: "A static linear solid mechanics analysis of a bearing block",
    resultIds: [
      "7f80757c-712f-4d1e-bb66-57a733d6769d",
      "67409c26-68f5-412a-ac98-ab07df4352bd",
      "85f5766b-b792-4de8-b8e2-293529841fa8"
    ],
    numberOfViews: 153,
    numberOfLikes: 0,
    numberOfGeometries: 1,
    numberOfMeshes: 1,
    numberOfSimulations: 1,
    numberOfSimulationRuns: 1,
    numberOfCopies: 3,
    thumbnailUrl: "/api/v1/projects/simscale/bearingblock-analysis/7f80757c-712f-4d1e-bb66-57a733d6769d/thumbnail?token\u003d3a7642b8787aab81d836e4e93c987d6b4f24a163834d28b657f44b67835f5da520160723T142000.000Z",
    categories: []
  },
  {
    projectName: "Bearingblock-Analysis",
    publicProjectName: "bearingblock-analysis",
    creationDate: "Nov 11, 2013 1:48:49 AM",
    lastModificationDate: "Feb 3, 2016 3:51:24 AM",
    ownerName: "rezendervp",
    parentProject: {
      projectName: "Bearingblock-Analysis",
      publicProjectName: "bearingblock-analysis_2",
      creationDate: "Aug 7, 2013 5:07:13 PM",
      lastModificationDate: "Mar 31, 2015 7:27:40 PM",
      ownerName: "simscale",
      resultThumbnailUrls: {}
    },
    measurementSystem: "SI",
    requesterPermission: {
      readPreview: false,
      read: false,
      write: false,
      billableAction: false,
      getCopy: false
    },
    publicPermission: {
      readPreview: true,
      read: true,
      write: false,
      billableAction: false,
      getCopy: true
    },
    description: "Fea analysis",
    resultIds: [
      "3f923e84-77db-4600-969a-07c5cad56675",
      "58ea9dae-bd5d-4aea-9797-c75a3736dc56",
      "e29c0eb9-6ec9-402e-bcd1-b8fef191094b",
      "3f923e84-77db-4600-969a-07c5cad56675",
      "7bddde78-f17d-4bbc-bbdc-fa6ab4aa5dda"
    ],    numberOfViews: 75,
    numberOfLikes: 0,
    numberOfGeometries: 1,
    numberOfMeshes: 3,
    numberOfSimulations: 3,
    numberOfSimulationRuns: 1,
    numberOfCopies: 0,
    thumbnailUrl: "/api/v1/projects/rezendervp/bearingblock-analysis/3f923e84-77db-4600-969a-07c5cad56675/thumbnail?token\u003d83f5beebdc5a7d1a12fa8aadbb2d94c27359c7cc1d56617901342f80824dfc4a20160723T142000.000Z",
    categories: []
  }
]
