import {expect} from 'chai';
import {reducer} from 'main/product/reducer';
import * as actionTypes from 'main/product/actionTypes';
import {PRODUCT_LIST_MOCK} from './product-mocks';

const ITEMS_PER_PAGE = 2;

describe('Products Reducer tests', () => {
  let currrentState = reducer();
  let query = {
    offset: 0,
    limit: ITEMS_PER_PAGE,
    order: 'desc',
    filterby: 'project_text',
    sortby: 'project_likes',
    filter: ''
  };

  it('SHOULD have initial state empty', () => {
    expect(reducer().productList).to.deep.equal([]);
  });

  describe('WHEN successfuly loading products', () => {
    const action = {
      type: actionTypes.GET_PRODUCTS_SUCCESS,
      result: {
        list: PRODUCT_LIST_MOCK,
        query
      }
    };

    beforeEach(() => {
      currrentState = reducer(currrentState, action);
    });

    it('SHOULD have those products in the list', () => {
      expect(currrentState.productList).to.deep.equal(PRODUCT_LIST_MOCK);
    });

    it('SHOULD have the query data saved', () => {
      expect(currrentState.queryData).to.deep.equal(query);
    });

    describe('WHEN starting to load more products', () => {
      const action = {
        type: actionTypes.LOAD_MORE_PRODUCTS
      };

      beforeEach(() => {
        currrentState = reducer(currrentState, action);
      });

      it('SHOULD have "isLoadingMoreProducts" set to true', () => {
        expect(currrentState.isLoadingMoreProducts).to.equal(true);
      });
    });

    describe('WHEN successfuly loaded more products', () => {
      const newQuery = {
        ...query,
        offset: 2
      };
      const action = {
        type: actionTypes.LOAD_MORE_PRODUCTS_SUCCESS,
        result: {
          list: PRODUCT_LIST_MOCK,
          query: newQuery
        }
      };

      beforeEach(() => {
        currrentState = reducer(currrentState, action);
      });

      it('SHOULD have "isLoadingMoreProducts" set to false', () => {
        expect(currrentState.isLoadingMoreProducts).to.equal(false);
      });

      it('SHOULD have "loadedLastProduct" set to false', () => {
        expect(currrentState.loadedLastProduct).to.equal(false);
      });

      it('SHOULD have the new items in the list', () => {
        const expectedResult = [
          ...PRODUCT_LIST_MOCK,
          ...PRODUCT_LIST_MOCK
        ];
        expect(currrentState.productList).to.deep.equal(expectedResult);
      });

      describe('WHEN again successfuly loaded more products and the last product was loaded', () => {
        const newQuery = {
          ...query,
          offset: 4
        };
        const action = {
          type: actionTypes.LOAD_MORE_PRODUCTS_SUCCESS,
          result: {
            list: [PRODUCT_LIST_MOCK[0]],
            query: newQuery
          }
        };

        beforeEach(() => {
          currrentState = reducer(currrentState, action);
        });

        it('SHOULD have "loadedLastProduct" set to false', () => {
          expect(currrentState.loadedLastProduct).to.equal(true);
        });

        it('SHOULD have 5 items in the list', () => {
          expect(currrentState.productList.length).to.deep.equal(5);
        });
      });
    });
  });
});
