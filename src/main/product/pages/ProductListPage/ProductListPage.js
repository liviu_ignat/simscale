import React, {Component, PropTypes} from 'react';
import Helmet from 'react-helmet';
import {connect} from 'react-redux';
import {push as pushState} from 'react-router-redux';
import {asyncConnect} from 'redux-async-connect';
import {ProductPageHeader, ProductList} from 'main/product/components';
import {
  getProductsAction,
  loadMoreProductsAction
} from 'main/product/actions';

@asyncConnect([{
  promise: ({location, store}) => {
    const {dispatch} = store;
    const filters = getUrlFilters(location);
    const promises = [
      dispatch(getProductsAction(filters))
    ];
    return Promise.all(promises);
  }
}])
@connect(
  ({product}) => ({
    products: product.productList,
    queryData: product.queryData,
    isLoadingMoreProducts: product.isLoadingMoreProducts,
    loadedLastProduct: product.loadedLastProduct
  }), {pushState, getProductsAction, loadMoreProductsAction})
export default class ProductListPage extends Component {
  static propTypes = {
    products: PropTypes.array.isRequired,
    queryData: PropTypes.object.isRequired,
    isLoadingMoreProducts: PropTypes.bool.isRequired,
    loadedLastProduct: PropTypes.bool.isRequired,
    location: PropTypes.any.isRequired,
    pushState: PropTypes.func.isRequired,
    getProductsAction: PropTypes.func.isRequired,
    loadMoreProductsAction: PropTypes.func.isRequired
  };

  searchAction(value) {
    const url = `/?search=${value}`;
    this.props.pushState(url);
  }

  render() {
    const css = require('./ProductListPage.scss');
    const {location, products, queryData, isLoadingMoreProducts, loadedLastProduct} = this.props;
    const {filter} = getUrlFilters(location);

    return (
      <div className={css.ProductListPage}>
        <Helmet title="SimScale - Public projects" />

        <div className={css.HeaderContainer_wrapper}>
          <div className={css.HeaderContainer}>
            <ProductPageHeader
              searchAction={::this.searchAction}
              searchValue={filter} />
          </div>
        </div>

        <div className={css.ListContainer}>
          <ProductList
            products={products}
            queryData={queryData}
            isLoadingMoreProducts={isLoadingMoreProducts}
            loadedLastProduct={loadedLastProduct}
            loadMoreProductsAction={this.props.loadMoreProductsAction} />
        </div>
      </div>
    );
  }
}

function getUrlFilters(location) {
  return {
    filter: getQueryParam(location, 'search')
  };
}

function getQueryParam(location, param) {
  if (location && location.query) {
    return location.query[param];
  }
}
