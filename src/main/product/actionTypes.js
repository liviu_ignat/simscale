export const GET_PRODUCTS = 'simscale/product/GET_PRODUCTS';
export const GET_PRODUCTS_SUCCESS = 'simscale/product/GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_FAIL = 'simscale/product/GET_PRODUCTS_FAIL';

export const LOAD_MORE_PRODUCTS = 'simscale/product/LOAD_MORE_PRODUCTS';
export const LOAD_MORE_PRODUCTS_SUCCESS = 'simscale/product/LOAD_MORE_PRODUCTS_SUCCESS';
export const LOAD_MORE_PRODUCTS_FAIL = 'simscale/product/LOAD_MORE_PRODUCTS_FAIL';
