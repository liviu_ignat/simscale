import * as actionTypes from './actionTypes';

const initialState = {
  productList: [],
  loadedLastProduct: false,
  isLoadingMoreProducts: false
};

export function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case actionTypes.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        productList: action.result.list,
        loadedLastProduct: action.result.list.length < action.result.query.limit,
        queryData: action.result.query
      };

    case actionTypes.LOAD_MORE_PRODUCTS:
      return {
        ...state,
        isLoadingMoreProducts: true
      };
    case actionTypes.LOAD_MORE_PRODUCTS_SUCCESS:
      return {
        ...state,
        productList: [
          ...state.productList,
          ...action.result.list
        ],
        queryData: action.result.query,
        loadedLastProduct: action.result.list.length < action.result.query.limit,
        isLoadingMoreProducts: false
      };
    case actionTypes.LOAD_MORE_PRODUCTS_FAIL:
      return {
        ...state,
        isLoadingMoreProducts: false
      };

    default:
      return {...state};
  }
}
