import * as actionTypes from './actionTypes';
import {app} from 'config';

const ITEMS_PER_PAGE = app.productPageSize;

export function getProductsAction({offset = 0, limit = ITEMS_PER_PAGE, order = 'desc', filterby = 'project_text', sortby = 'project_likes', filter = ''} = {}) {
  const url = `/api/v1/projects?limit=${limit}&offset=${offset}&order=${order}&filterby=${filterby}&sortby=${sortby}&filter=${filter}`;

  return {
    types: [
      actionTypes.GET_PRODUCTS,
      actionTypes.GET_PRODUCTS_SUCCESS,
      actionTypes.GET_PRODUCTS_FAIL
    ],
    promise: client => client.get(url).then(list => {
      return {
        list,
        query: {
          filter,
          offset,
          limit,
          sortby,
          filterby,
          order
        }
      };
    })
  };
}

export function loadMoreProductsAction(queryData = {}) {
  const query = { ...queryData };
  query.offset = query.offset + ITEMS_PER_PAGE;

  const {offset, limit, order, filterby, sortby, filter} = query;
  const url = `/api/v1/projects?limit=${limit}&offset=${offset}&order=${order}&filterby=${filterby}&sortby=${sortby}&filter=${filter}`;

  return {
    types: [
      actionTypes.LOAD_MORE_PRODUCTS,
      actionTypes.LOAD_MORE_PRODUCTS_SUCCESS,
      actionTypes.LOAD_MORE_PRODUCTS_FAIL
    ],
    promise: client => client.get(url).then(list => {
      return {
        list,
        query: {
          filter,
          offset,
          limit,
          sortby,
          filterby,
          order
        }
      };
    })
  };
}

