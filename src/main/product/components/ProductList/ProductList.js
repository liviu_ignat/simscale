import React, {Component, PropTypes} from 'react';
import {ProductCard} from 'main/product/components';
import {RefreshIndicator} from 'main/common/components';

export default class ProductList extends Component {
  static propTypes = {
    products: PropTypes.array.isRequired,
    queryData: PropTypes.object.isRequired,
    isLoadingMoreProducts: PropTypes.bool.isRequired,
    loadedLastProduct: PropTypes.bool.isRequired,
    loadMoreProductsAction: PropTypes.func.isRequired
  };

  onLoadMoreClick() {
    const {queryData, loadMoreProductsAction} = this.props;
    loadMoreProductsAction(queryData);
  }

  render() {
    const css = require('./ProductList.scss');
    const {products, isLoadingMoreProducts, loadedLastProduct} = this.props;

    return (
      <div>
        <div className={css.ProductList}>
          {products.map((product, index) => (<div key={index} className={css.ProductRow}>
              <ProductCard
                product={product} />
            </div>))}
        </div>

        {!loadedLastProduct && <div className={css.LoadMoreContainer}>
          {isLoadingMoreProducts && (<div className={css.RefreshIndicatorContainer}>
            <RefreshIndicator
              size={48}
              left={0}
              top={0}
              status="loading" />
          </div>)}

          {!isLoadingMoreProducts && <button
            className={css.LoadMoreButton}
            onClick={::this.onLoadMoreClick}>
              <span>Show more results</span>
            </button>}

          <div className={css.clearfix}/>
        </div>}
      </div>
    );
  }
}
