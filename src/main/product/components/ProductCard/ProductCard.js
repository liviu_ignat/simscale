import React, {Component, PropTypes} from 'react';
import {formatUrl, formatDateFromNow} from 'helpers/formatters';

export default class ProductCard extends Component {
  static propTypes = {
    product: PropTypes.object.isRequired
  };

  render() {
    const css = require('./ProductCard.scss');
    const userProfileImage = require('./images/user_profile.png');
    // const eyeImage = require('./images/eye.png');
    const {product} = this.props;
    const {
      projectName,
      publicProjectName,
      thumbnailUrl,
      ownerName,
      numberOfGeometries,
      numberOfMeshes,
      numberOfSimulations,
      lastModificationDate,
      numberOfViews,
      numberOfLikes,
      numberOfCopies
    } = product;
    const projectUrl = `/projects/${ownerName}/${publicProjectName}/`;
    const imageUrl = thumbnailUrl || '/community/assets/img/no-preview-gray.png';
    const meshText = numberOfMeshes === 1 ? 'Mesh' : 'Meshes';
    const simulationText = numberOfSimulations === 1 ? 'Simulation' : 'Simulations';
    const modelsText = numberOfGeometries === 1 ? 'model' : 'models';

    return (
      <div className={css.ProductCard}>
        <a className={css.ProjectImageLink} href={formatUrl(projectUrl)}>
          <div className={css.ProjectImageLink_hoverContainer}>
            <span className="fa fa-eye" />
          </div>
          <img className={css.ProductCardImage} src={formatUrl(imageUrl)} alt="" />
        </a>
        <div className={css.ProductCardContent}>
          <div className={css.TitleAndUserImage}>
            <img className={css.UserImage} alt="" src={userProfileImage}/>
            <div className={css.TitleContent}>
              <a className={css.TitleLink} href={formatUrl(projectUrl)}>{projectName}</a>
              <div className={css.ProductOwnerName}>by {ownerName}</div>
            </div>
          </div>
          <div className={css.NumberOfData}>
            {numberOfGeometries} CAD {modelsText} / {numberOfMeshes} {meshText} / {numberOfSimulations} {simulationText}
          </div>
        </div>
        <div className={css.ProductCardFooter}>
          <span className={css.CardDate}>Changed {formatDateFromNow(lastModificationDate)}</span>
          <span className={css.CardViewsAndLikes}>
            <span className="fa fa-eye"></span>&nbsp;{numberOfViews}&nbsp;
            <span className="fa fa-heart"></span>&nbsp;{numberOfLikes}&nbsp;
            <span className="fa fa-code-fork"></span>&nbsp;{numberOfCopies}&nbsp;
          </span>
        </div>
      </div>
    );
  }
}
