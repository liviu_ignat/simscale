import React, {Component, PropTypes} from 'react';
import {TextFieldGroup, ButtonGroup, ButtonGroupItem} from 'main/common/components';

export default class ProductPageHeader extends Component {
  static propTypes = {
    searchValue: PropTypes.string,
    searchAction: PropTypes.func.isRequired
  };

  onSearchButtonClick(searchValue) {
    this.props.searchAction(searchValue);
  }

  render() {
    const css = require('./ProductPageHeader.scss');
    const {searchValue} = this.props;
    const buttonLabelStyle = {
      padding: '0 4px',
      minWidth: 20
    };

    return (
      <div className={css.ProductPageHeader}>
        <h3 className={css.HeaderTitle}>Public Projects</h3>
        <div className={css.RightFieldsContainer}>
          <div className={css.SearchContainer}>
            <TextFieldGroup
            value={searchValue}
              inputPlaceholder="Search..."
              buttonLabel={<span className="fa fa-search" style={buttonLabelStyle}/>}
              onSubmit={::this.onSearchButtonClick} />
          </div>

          <div className={css.ButtonsContainer}>
            <div className={css.ButtonGroupContainer}>
              <ButtonGroup>
                <ButtonGroupItem><span className="fa fa-eye" style={buttonLabelStyle}/></ButtonGroupItem>
                <ButtonGroupItem><span className="fa fa-heart" style={buttonLabelStyle}/></ButtonGroupItem>
                <ButtonGroupItem><span className="fa fa-code-fork" style={buttonLabelStyle}/></ButtonGroupItem>
                <ButtonGroupItem><span className="fa fa fa-pencil-square-o" style={buttonLabelStyle}/></ButtonGroupItem>
              </ButtonGroup>
            </div>

            <div className={css.ButtonGroupContainer}>
              <ButtonGroup>
                <ButtonGroupItem><span className="fa fa-th-list" style={buttonLabelStyle}/></ButtonGroupItem>
              </ButtonGroup>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
